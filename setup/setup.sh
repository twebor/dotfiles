#!/bin/sh
DOTFILES="~/.dotfiles" ./setup.sh

if [[ ! -d "$DOTFILES" ]]
then
    echo "Cloning dotfiles"
	git clone git@bitbucket.org:twebor/dotfiles.git "$DOTFILES" 
fi

if [ -ne ~/.vim/bundle/Vundle.vim ]
then
	echo "Installing Vundle"
	# Install Vundle
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
else
	echo "Vundle already exists!"
fi

# Create local .vimrc that sources repo.
if [ -ne ~/.vimrc ]
then
    echo "Generating default .vimrc"
    echo "source ~/.dotfiles/.vimrc" > ~/.vimrc
else
    echo ".vimrc already exists! Skipping generation."
fi

