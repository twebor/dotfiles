""""""""""""""""""""
" General settings "
"                  "
""""""""""""""""""""
" load plugins from vundle
source $HOME/.dotfiles/plugins.vim

" Include all .vim files in functions folder. https://stackoverflow.com/questions/4500748/how-to-source-all-vim-files-in-directory" Load functions
for f in split(glob('$HOME/.dotfiles/functions/*.vim'), '\n')
    exe 'source' f
endfor

" Automatically jump to last known location in vim. https://askubuntu.com/questions/202075/how-do-i-get-vim-to-remember-the-line-i-was-on-when-i-reopen-a-file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

" Leader
let mapleader=","

" Whitespace
set wrap
" set textwidth=79
set noshiftround

" Spaces and tabs
filetype plugin indent on " https://stackoverflow.com/questions/234564/tab-key-4-spaces-and-auto-indent-after-curly-braces-in-vim
set shiftwidth=4
set softtabstop=4
set tabstop=4 " tabs appear as 4 spaces
set expandtab " tabs are spaces
set list lcs=tab:\|\ " add indentation indicators on tabs
set autoindent
set nostartofline

" Lines
set number
set relativenumber
set cursorline " highlights current line
set showcmd " shows command in bottom-right

" Folding
set foldenable
set foldlevelstart=10 " open most folds by default
set foldnestmax=10

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" Mouse settings
set mouse=a " enable use of mouse for all modes

" Command Window Settings
set wildmenu " visual autocomplete for command menu
set confirm
set ruler " display cursor position on command bar
set cmdheight=2

" Scrolling
set scrolloff=8 " start scrolling 8 lines away from margins

" Encoding
set fileencoding=utf-8
set encoding=utf-8

" Security
set modelines=0

" Backups
set noswapfile
set nobackup
set nowritebackup

" Autoread - Doesn't work as well.  Use :WatchForChanges function
" au FocusLost,WinLeave * :silent! noautocmd w " Autoread whenever buffer changes https://stackoverflow.com/questions/2490227/how-does-vims-autoread-work
" au FocusGained,BufEnter * :silent! !
set autoread

" Misc
set hidden " buffers can exist in the bg without being in a window
set history=1000
set updatetime=100 " Git gutter update quicker
set lazyredraw " redraw only when needed
set backspace=indent,eol,start " Allow backspacing over autoindent, line breaks and start of insert action
set visualbell " Disable system bell
set notimeout ttimeout ttimeoutlen=200 " Quickly time out on keycodes, but never time out on mappings 
" set clipboard=unnamed " copy to system clipboard

"""""""""""""""""""
" Search Settings "
"                 "
"""""""""""""""""""
set ignorecase
set smartcase " only ignore case when all lowercase
set showmatch " highlight matching [{()}]
set incsearch " search as characters are entered
set hlsearch " highlight matches
map <leader><space> :let @/=''<cr> " clear search

"""""""""""""""""""""
" General Shortcuts "
"                   "
"""""""""""""""""""""
set pastetoggle=<F2>

""""""""""""""""""
" Color Settings "
"                "
""""""""""""""""""
colorscheme gruvbox
"let g:airline_theme='gruvbox'
set background=dark

" Airline arrow fix
let g:airline_right_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_left_alt_sep= ''
let g:airline_left_sep = ''

syntax enable " enable syntax processing

"""""""""""""""""""
" Plugin Settings "
"                 "
"""""""""""""""""""
" NERDTREE
nmap <leader>ne :NERDTree<cr>

" IndentLine
let g:indentLine_color_term = 239 " Set indent line color to gray

" GitGutter
highlight clear SignColumn
highlight GitGutterAdd ctermfg=green
highlight GitGutterChange ctermfg=yellow
highlight GitGutterDelete ctermfg=red
highlight GitGutterChangeDelete ctermfg=yellow

" Emmet
let g:user_emmet_mode='i' " Only in insert mode
let g:user_emmet_install_global = 0 " Only in html/css files
autocmd FileType html,css EmmetInstall
autocmd FileType html,css imap <TAB> <plug>(emmet-expand-abbr)

" Tagbar
nmap <F8> :TagbarToggle<CR>
" let g:tagbar_ctags_bin='$HOME/.vim/ctags-5.8' " installed system-wide

" YouCompleteMe
" let g:ycm_confirm_extra_conf=0

" Snippets
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
" Set ultisnips triggers
let g:UltiSnipsExpandTrigger="<tab>"                                            
let g:UltiSnipsJumpForwardTrigger="<tab>"                                       
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"                                    

