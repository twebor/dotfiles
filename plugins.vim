set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" = ESSENTIAL PLUGINS = "
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" Plugin 'ervandew/supertab' " Tab completion
Plugin 'tpope/vim-fugitive' " Git integration
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/nerdtree' " File explorer
Plugin 'Xuyuanp/nerdtree-git-plugin' " Nerdtree git integration
Plugin 'yggdroot/indentline' " Needs >= vim v7.3
Plugin 'ctrlpvim/ctrlp.vim' " Fuzzy file searching
Plugin 'tpope/vim-repeat' " Fix '.' repeat on some plugins
Plugin 'jiangmiao/auto-pairs'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdcommenter'

" C++
Plugin 'derekwyatt/vim-fswitch'

" Utility
Plugin 'tpope/vim-surround'
Plugin 'majutsushi/tagbar'

" Form
" Plugin 'yuttie/comfortable-motion.vim'

" Completion
" Plugin 'rdnetto/ycm-generator'
" Plugin 'valloric/youcompleteme'

" Snippets
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'

" Gruvbox theme
Plugin 'morhetz/gruvbox'

" Powerline
Plugin 'vim-airline/vim-airline'

" Tmux
Plugin 'benmills/vimux'

" Web-development
Plugin 'mattn/emmet-vim'
Plugin 'othree/html5.vim'

" Javascript
Plugin 'pangloss/vim-javascript'
Plugin 'elzr/vim-json'
Plugin 'leafgarland/typescript-vim'

" CSS
Plugin 'ap/vim-css-color'

" Markdown
Plugin 'plasticboy/vim-markdown'

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
